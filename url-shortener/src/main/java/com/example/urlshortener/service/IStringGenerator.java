package com.example.urlshortener.service;

public interface IStringGenerator {
    public String generate();
}
