package com.example.urlshortener.service;

import com.example.urlshortener.model.TinyUrlResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TinyUrlService {
    @Autowired
    private IStringGenerator stringGenerator;

    private final String mainUrl="http:://localhost:8080/tiny-url/";
    private Map<String, String> urlData;
    private String chars = "abcdefghijklmnopqrstuvwxyz0123456789";

    public TinyUrlService(){
        urlData = new HashMap<>();
    }

    public TinyUrlResponse convertUrl(String originalUrl){
        String convertedUrlUUID = urlData.getOrDefault(originalUrl.trim(), null);

        if(convertedUrlUUID != null)
            throw new RuntimeException("This url is converted!!!");
        String generatedUrl = "";
        while(true){
           generatedUrl = stringGenerator.generate();
            if( !urlData.values().contains(generatedUrl)){
                break;
            }
        }

        urlData.put(originalUrl, generatedUrl);

        return new TinyUrlResponse(originalUrl, getConvertedUrl(originalUrl));
    }

    public String getConvertedUrl(String originalUrl){
        String absoluteUrl = urlData.getOrDefault(originalUrl, null);
        if(absoluteUrl != null){
            return this.mainUrl + "" + absoluteUrl;
        }

        return null;
    }

    public String getOrginalUrl(String convertedUrl){
        return urlData
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().equals(convertedUrl))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(null);
    }
}
