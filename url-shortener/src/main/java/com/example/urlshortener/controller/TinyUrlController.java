package com.example.urlshortener.controller;

import com.example.urlshortener.model.TinyUrlRequest;
import com.example.urlshortener.model.TinyUrlResponse;
import com.example.urlshortener.service.TinyUrlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/tiny-url")
@Api(value = "TinyUrl api documentation")
public class TinyUrlController {

    @Autowired
    private TinyUrlService tinyUrlService;

    @PostMapping
    @ApiOperation(value = "The given url will be converted and registered")
    public ResponseEntity<TinyUrlResponse> createTinyUrl(@RequestBody TinyUrlRequest request){

        TinyUrlResponse tinyUrlResponse = tinyUrlService.convertUrl(request.getOriginalUrl());
        return ResponseEntity.ok(tinyUrlResponse);
    }

    @GetMapping("/{url}")
    @ApiOperation(value="Url will be redirected to original url")
    public ResponseEntity redirectUrl(@PathVariable String url){
        Optional<String> orginalUrl = Optional.of(tinyUrlService.getOrginalUrl(url));
        if(!orginalUrl.isPresent()){
           return ResponseEntity.notFound().build();
        }

        return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(orginalUrl.get())).build();
    }
}
