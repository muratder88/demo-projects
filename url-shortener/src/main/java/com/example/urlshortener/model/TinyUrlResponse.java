package com.example.urlshortener.model;

public class TinyUrlResponse {
    private String orginalUrl;
    private String convertedUrl;

    public TinyUrlResponse(String orginalUrl, String convertedUrl) {
        this.orginalUrl = orginalUrl;
        this.convertedUrl = convertedUrl;
    }

    public String getOrginalUrl() {
        return orginalUrl;
    }

    public void setOrginalUrl(String orginalUrl) {
        this.orginalUrl = orginalUrl;
    }

    public String getConvertedUrl() {
        return convertedUrl;
    }

    public void setConvertedUrl(String convertedUrl) {
        this.convertedUrl = convertedUrl;
    }
}
