package com.example.urlshortener.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="Tiny Url request documentation", description = "Tiny url post request model")
public class TinyUrlRequest {
    @ApiModelProperty(value = "Url field that the user wants to convert")
    private String originalUrl;

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }
}
