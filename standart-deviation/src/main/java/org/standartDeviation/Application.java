package org.standartDeviation;

public class Application {

    public static void main(String[] args){
        StandartDeviationService standartDeviationService = new StandartDeviationService();
        short[] sayilar = {10, 8, 10, 8, 8, 4};

        double result = standartDeviationService.standartSapmaHesapla(sayilar, (short)2);
        if(result == 2.19){
            System.out.println("Test passed");
        } else {
            throw new RuntimeException("Test failed. Try again");
        }

    }
}
