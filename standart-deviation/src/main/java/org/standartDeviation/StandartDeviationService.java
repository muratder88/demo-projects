package org.standartDeviation;

import java.text.NumberFormat;
import java.util.*;


public class StandartDeviationService {
    private List<Short> sayiList;
    private List<DeviationThread> threadList;
    private short result;

    public double standartSapmaHesapla(short[] sayilar, short threadSayisi)  {
        this.threadList = new ArrayList<>();

        this.setSayilar(sayilar);

        int elemanCount= this.getCountOfSayilar() / (int) threadSayisi;
        for(int i = 0; i < threadSayisi; i++){
            List<Short> shorts = null;
            if (i == threadSayisi-1){
                shorts = sayiList.subList(i * elemanCount, this.getCountOfSayilar());
            } else {
                shorts = sayiList.subList(i * elemanCount,  elemanCount + (i * elemanCount));
            }

            this.startThread(shorts);
        }

        this.addThreadResults();
        return this.getFormattedValue();
    }

    private void startThread(Collection<Short> sayilar){
        DeviationThread thread = new DeviationThread(Collections.unmodifiableCollection(sayiList), Collections.unmodifiableCollection(sayilar));
        thread.start();
        threadList.add(thread);
    }

    private double getFormattedValue(){
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        String valueWithoutRounded = nf.format(Math.sqrt(result /(double) (sayiList.stream().count() -1)));
        return Double.parseDouble(valueWithoutRounded);
    }

    private void addThreadResults(){
        result = 0;
        for (DeviationThread thread: threadList) {
            try {
                thread.join();
                result += (Short) thread.getTotal();
                //System.out.println("Total is " + thread.getTotal() + " Thread Name is " + thread.getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void setSayilar(short[] sayilar){
        this.sayiList = new ArrayList<>();
        for (short sayi : sayilar) {
            this.sayiList.add(sayi);
        }
    }

    private int getCountOfSayilar(){
        return (int) sayiList.stream().count();
    }
}
