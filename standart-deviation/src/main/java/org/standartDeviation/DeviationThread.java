package org.standartDeviation;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class DeviationThread extends Thread {
    private Collection<Short> sayilar;
    private Collection<Short> subSayilar;
    private short average = 0;
    public AtomicReference<Short> total = new AtomicReference<>((short) 0);


    public DeviationThread(){
    }


    public DeviationThread(Collection<Short> sayilar, Collection<Short> subSayilar){
        this.sayilar=sayilar;
        this.subSayilar = subSayilar;
    }

    public void calculateAvarage(){
        AtomicReference<Short> total = new AtomicReference<>((short) 0);
        sayilar
                .stream()
                .forEach(sayi -> {
                    total.set((short)(total.get() + sayi));
                });
        this.average =  (short) (total.get() / (short) this.sayilar.stream().count());
    }

    public Short getTotal() {
        return total.get();
    }

    @Override
    public void run() {
        this.calculateAvarage();


        subSayilar.stream()
                .forEach(sayi ->{
                    short number = (short) (this.average - sayi);
                    total.set((short) (total.get() + (number * number)));
                });
        String join = subSayilar.stream().map(Object::toString).collect(Collectors.joining(","));
        System.out.println("Thread Name: " + currentThread().getName() + "sub sayilar :: " + join+ " average: " + average + " total:: " + total.get());

    }
}
